ESE es una **librería** orientada al **desarrollo de videojuegos** en C++. Se apoya sobre la librería [SFML ](http://www.sfml-dev.org/)para trabajar con elementos multimedia. A esto, ESE añade ciertas utilidades para el desarrollo de juegos, como la gestión de escenas, gestión de recursos, etc.

ESE está compuesto de varios módulos:

-**Core**: es el núcleo de la librería, tiene clases para crear y gestionar escenas (Scene y SceneManager), para gestionar recursos (ResourceManager), entre otras cosas.

-**EntitySystem**: para el desarrollo orientado a Entidades y Componentes.

-**Mesh**: creación de mallas y búsqueda del camino más corto en ellas.

-**TileEngine**: herramienta para crear escenarios basados en casillas.

-**Text**: sistema sencillo de internacionalización.