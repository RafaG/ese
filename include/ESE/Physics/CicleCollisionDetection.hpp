/* 
 * File:   CicleCollisionDetection.hpp
 * Author: Rafa
 *
 * Created on March 1, 2015, 10:36 PM
 */

#ifndef CICLECOLLISIONDETECTION_HPP
#define	CICLECOLLISIONDETECTION_HPP

#include <cmath>

class CircleCollisionDetection
{
public:
    bool collision(float x1, float y2, float radius, float px, float py);
};

#endif	/* CICLECOLLISIONDETECTION_HPP */

